<?php
return [
  'backend' => 
  [
    'frontName' => '{link ke backend}',
  ],
  'install' => 
  [
    'date' => 'Sun, 24 Apr 2016 09:41:49 +0000',
  ],
  'crypt' => 
  [
    'key' => 'mdpxjZVm1wedUZf2M886',
  ],
  'session' => 
  [
    'save' => 'files',
  ],
  'db' => 
  [
    'table_prefix' => '',
    'connection' => 
    [
      'default' => 
      [
        'host' => 'localhost',
        'dbname' => '{nama database yang dibuat}',
        'username' => '{nama user database yang dibuat}',
        'password' => '{password user database yang dibuat}',
        'model' => 'mysql4',
        'engine' => 'innodb',
        'initStatements' => 'SET NAMES utf8;',
        'active' => '1',
      ],
    ],
  ],
  'resource' => 
  [
    'default_setup' => 
    [
      'connection' => 'default',
    ],
  ],
  'x-frame-options' => 'SAMEORIGIN',
  'MAGE_MODE' => 'production',
  'cache_types' => 
  [
    'config' => 0,
    'layout' => 0,
    'block_html' => 0,
    'collections' => 0,
    'reflection' => 0,
    'db_ddl' => 0,
    'eav' => 0,
    'config_integration' => 0,
    'config_integration_api' => 0,
    'full_page' => 0,
    'translate' => 0,
    'config_webservice' => 0,
    'compiled_config' => 1,
    'customer_notification' => 0,
    'autocancelorder_cache_tag' => 0,
  ],
];